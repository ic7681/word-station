const TextCard = ({ text }: { text: string }) => (
  <li className="rounded-lg p-6 bg-slate-800">{text}</li>
);

export default TextCard;
